import React, {Component} from "react";
import { connect } from 'react-redux';

class Item extends Component {
    handleClick = () => {
        this.props.dispatch( {
            type:'addToChart',
            name: this.props.text,
            id: this.props.id,
        })
    };

    render(){
        return(
            <div className="Itemborder">
                <img src={this.props.img} alt="Smiley face" height = "160px"/>
                <li onClick= {this.handleClick}> {this.props.text} </li>
            </div>
        )
    }

}




export default connect()(Item);
