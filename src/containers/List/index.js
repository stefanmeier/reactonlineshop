import React, {Component} from "react";
import { connect } from 'react-redux';
import Item from '../../components/Item'



class List extends Component {
    render(){
        console.log("render!")
        return(
            <ul>
                {
                    this.props.items.map(item => <Item key = {item.name} text = {item.name} id = {item.id} img = {item.img}/>)
                }
            </ul>
        )
    }

}

const mapStateToProps = state => {
  return {
    items: state.items
  }
};

export default connect(mapStateToProps)(List);
