import React, {Component} from 'react';
import List from '../List'
import Shoppingchart from '../Shoppingchart'


import './index.css';

class App extends Component {
    render() {
        return (
            <div>
                <List/>
                <Shoppingchart/>
            </div>
        );
    }
}



export default App;
