import React, {Component} from "react";
import { connect } from 'react-redux';

class ChartItem extends Component {

    render(){
        return(
                <li> {this.props.text}, {this.props.counter}</li>
        )
    }

}

export default connect()(ChartItem);
