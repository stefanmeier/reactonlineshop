import React, {Component} from "react";
import { connect } from 'react-redux';
import ChartItem from '../ChartItem';



class Shoppingchart extends Component {
    render(){
        console.log("RENDER!!");
            //let inChart = ;
            //let inChart = this.props.items.filter(x => x.counter >= 1);
        return(
            <ul>{
                this.props.items.map(item => <ChartItem key = {item.name} text = {item.name} id = {item.id} img = {item.img} price = {item.price} counter = {item.counter}/>)
                    //inChart.map(item => <ChartItem key = {item.name} text = {item.name} id = {item.id} img = {item.img} price = {item.price} counter = {item.counter}/>)
            }</ul>
        )
    }

}

const mapStateToProps = (state) => {
  return {
    items: state.items
  }
};

export default connect(mapStateToProps)(Shoppingchart);
