import { createStore } from 'redux'

const initialState = {
    items: [
        {name : 'blue shirt',
         id : '1',
         img : 'https://cdn.shopify.com/s/files/1/2959/1448/products/prestige_20worldwide_20light_20blue_20shirt_720x.png?v=1540820905',
         price : 150,
         counter : 0,
        },
        {name : 'wolf shirt',
         id : '2',
         img : 'https://cdn.shopify.com/s/files/1/2959/1448/products/wolfflipmens_720x.jpg?v=1540811467',
         price : 150,
         counter : 0,
        },
        {name : 'bear shirt',
         id : '3',
         img : 'https://cdn.shopify.com/s/files/1/2959/1448/products/papa_20bear_20dark_20heather_20grey_20shirt_1b00e5fe-87f8-4d4a-ae52-54dc41641aa3_720x.png?v=1541028387',
         price : 150,
         counter : 0,
        }
        ]
};

/*const reducer = (state = initialState, action) => {
    let newState = {...state};
    switch (action.type) {
        case 'addToChart': {
            newState.items.find(x => x.id === action.id).counter += 1;
            // console.log(newObject.items.find(x => x.id === action.id).counter);
            console.log(newState);
            return newState;
        }
        default:
            return state
    }
};*/

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'addToChart': {
            const items = state.items.map(item => {
                if (item.id === action.id) {
                    item.counter += 1
                }
                return item
            });

            const newState = { ...state };
            newState.items = items;
            return newState;

            // return {...state, items: items};
        }
        default:
            return state
    }
};




const store = createStore(reducer);

export default store